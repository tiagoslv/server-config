#!/bin/sh

# This script imports all MySQL dumps in the directory ".restore".
# The MySQL dump file names should correspond to the relevant database names.

su_cmd="su root"
docker_cmd="docker-compose"
mysql_cmd="/usr/bin/mysql --defaults-extra-file=/tmp/my.cnf"

container_dir=`pwd`
dump_paths=`$su_cmd -c "find .restore -type f -name *.sql -printf \"%p\n\""`

# Check if there are any MySQL dumps to restore
if [ -z "$dump_paths" ]; then
    echo "${container_dir}: No MySQL dump files found"
    exit 1
fi

# Check if there is a Docker service called "mysql" running
if ! $su_cmd -c "$docker_cmd exec -T mysql /bin/bash -c 'echo 1'" >/dev/null 2>&1; then
    echo "${container_dir}: No MySQL service running"
    exit 1
fi

exit_code=0

# Create a temporary my.cnf file inside the container
$su_cmd -c "$docker_cmd exec -T mysql /bin/bash -c 'printf \"# Auto-generated backupninja MySQL conf\n\
[mysql]\n\
host=localhost\n\
user=\`printenv MYSQL_USER\`\n\
password=\`printenv MYSQL_PASSWORD\`\n\
\n\
[mysqldump]\n\
host=localhost\n\
user=\`printenv MYSQL_USER\`\n\
password=\`printenv MYSQL_PASSWORD\`\n\
\" > /tmp/my.cnf'"

for dump_path in $dump_paths; do
    dump_file="${dump_path##*/}" # Get everything after last "/" (file name with extension)
    database="${dump_file%%.*}" # Get everything before first "." (file's name)

    # Import MySQL dump
    output=`$su_cmd -c "set -o pipefail; cat $dump_path | $docker_cmd exec -T mysql /bin/bash -c '$mysql_cmd $database'" 2>&1`
    code=$?

    if [ $code -ne 0 ]; then
        echo "${container_dir}: $output"
        echo "${container_dir}: Failed to restore database $database"

        # Random exit code other than 1
        exit_code=126
    else
        echo "${container_dir}: $output"
        echo "${container_dir}: Successfully restored database $database"

        # Delete MySQL dump file
        $su_cmd -c "rm $dump_path"
    fi
done

# Clean up temporary files inside container
$su_cmd -c "$docker_cmd exec -T mysql /bin/bash -c 'rm /tmp/my.cnf'"

# Remove .restore directory (if empty)
$su_cmd -c "rmdir .restore" >/dev/null 2>&1

exit $exit_code
