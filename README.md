
# Server config

Turn a fresh OS installation into a fully functional web server. It uses [Ansible] to install and configure all the required software on remote hosts. Tested on **Ubuntu Server 16.04.4 LTS**.

- [Server config](#server-config)
  - [Get started](#get-started)
  - [Set up an SSH agent](#set-up-an-ssh-agent)
  - [Add a new server](#add-a-new-server)
  - [Roles available](#roles-available)
  - [Important notes](#important-notes)
  - [Directory structure](#directory-structure)
  - [References](#references)


## Get started

1. [Install Ansible](http://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html) on the local host

2. Clone this repository

        $ git clone git@bitbucket.org:tiagoslv/server-config.git

3. Create the Ansible inventory file

        $ cd server-config
        $ cp hosts.example hosts

    Use `hosts.example` as a reference. Don't forget to remove the example entries.


## Set up an SSH agent

This allows remote hosts to authenticate with third-party hosts using SSH keys on the local host.

1. Start `ssh-agent` in the background

        $ eval "$(ssh-agent -s)"

2. If you're using **macOS Sierra 10.12.2 or later**, add the following to your `~/.ssh/config`

        Host *
          AddKeysToAgent yes
          UseKeychain yes
          IdentityFile ~/.ssh/id_rsa

    It will automatically load keys into the SSH agent and store passphrases in your keychain.

3. Add your SSH private key to the SSH agent

        $ ssh-add ~/.ssh/id_rsa

    Add the `-K` flag if you're on Mac OS X. Otherwise, `ssh-agent` will "forget" this key once the machine is rebooted.

Tip: to use the SSH agent out of Ansible, run `ssh user@host -o ForwardAgent=yes`.


## Add a new server

1. Add a new entry to the `hosts` inventory file

        [docker]
        hostname  ansible_host=10.20.30.40 ansible_port=22 ansible_user=root

    Make sure you choose the right group ("docker" in the example). Check the `webservers.yml` playbook file to know what each group does.

2. Create a new host variables file

        $ cd host_vars
        $ cp hostname.yml.example hostname.yml
        $ nano hostname.yml

    The file name should be the same as the host name in the inventory. It will contain all the required configuration settings for the new server. Use `hostname.yml.example` as a reference.

3. Make sure there are no SSH expired password prompts

    You might be required to change the user password when you log in for the first time. This can cause Ansible to hang. To prevent it, manually SSH into the machine and change the password to a temporary one.

4. Run the `webservers.yml` playbook

        $ ansible-playbook webservers.yml

    Add `--ask-pass` to the end if you need to provide a password to log in via SSH.

    **Important:** Ansible will change the root password and output it. Please take note of it.

    If the playbook fails, run it again with `--start-at-task="Task in some role"` to start executing the playbook at a particular task. You might need to follow the next step for this.

5. Update the inventory file

    Ansible will switch to a new user throughout the play. If you want to run this or another playbook against this host in the future, make sure you update `ansible_user` in the inventory file to reflect the new user details. You might need to set `ansible_become` and `ansible_become_user` too.

        [docker]
        hostname  ansible_host=10.20.30.40 ansible_port=22 ansible_user=foo ansible_become=yes ansible_become_user=root
        
    If not, just comment out or remove the entry.

        [docker]
        # hostname  ansible_host=10.20.30.40 ansible_port=22 ansible_user=root


## Roles available

- **Core setup**
    - Sets hostname and timezone
    - Upgrades all installed packages to their latest versions
    - Installs Pip, Zip, Unzip and Fail2ban packages
    - Updates root password and creates new user accounts with sudo access and authorised SSH public keys
    - Disables root SSH login and password-based authentication, and enables SSH agent forwarding
- **Firewall**
    - Installs and configures UFW
    - Sets firewall rules
- **Logrotate**
    - Installs and configures Logrotate
- **Backupninja**
    - Generates or uses existing GnuPG keys
    - Installs Duplicity and Backupninja
    - Configures Backupninja to schedule full and incremental backups
    - Restore any existing backups from Amazon S3
- **Web sites**
    - Configures Nginx and PHP-FPM and creates web root directory structure owned by www-data
    - Pulls relevant source code from Git (if no backup has been restored)
    - Restores database backups (if a backup has been restored)
    - Sets permissions for web root directories
    - Sets up and starts Docker containers for each web site [^1]
- **Docker**
    - Installs and starts Docker, and configures it to start on boot
    - Installs Docker Compose
    - Sets up cron job to prune images, containers, networks and volumes
- **Nginx reverse proxy** [^1]
    - Creates an external Docker network
    - Sets up and starts Nginx reverse proxy container
    - Ensures www-data user and group exist
- **InvoicePlane** [^1]
    - Configures Nginx and PHP-FPM and creates web root directory structure owned by www-data
    - Pulls InvoicePlane source code as a ZIP file and custom templates from Git (if no backup has been restored)
    - Restores database backup (if a backup has been restored)
    - Sets permissions for web root directory
    - Sets up and starts Docker containers
- **Jenkins** [^1]
    - Sets up and starts a Docker container with Jenkins
- **Reboot**
    - Reboots system (if required) and waits for it to boot back up


## Important notes

- Custom facts are cached for 2 hours (some tasks might fail if executed after the 2 hour window)
- Data is pulled from one of the following sources, by order of priority:
    1. Backups
    2. Git repositories
- Configuration files are always overwritten (they should be managed by Ansible only)
- All data managed by Docker is mounted in volumes, so that it doesn't get lost when containers are recreated
- The default directory permissions are strict and may have to be changed in some scenarios (e.g. upload directories)


## Directory structure

    server-config/
    ├── group_vars/
    |   └── all.yml
    ├── host_vars/
    |   └── hostname.yml.example
    ├── roles/
    |   ├── core/
    |   ├── firewall/
    |   └── ...
    ├── ansible.cfg
    ├── hosts.example
    └── webservers.yml

An overview of what each file/directory means:

File / directory    | Description
:------------------ | :--
`group_vars/`       | Group variables directory. The file `all.yml` contains settings available to all hosts. [Read more][ansible-playbooks-variables] about variables in playbooks.
`host_vars/`        | Host variables directory. Each file in it contains all the required configuration for a specific host – `hostname.yml.example` is an example. The file naming convention is `hostname.yml`, where "hostname" corresponds to a host name in the inventory file.
`roles/`            | Roles directory. Each subdirectory represents a role with tasks, handlers, etc. [Read more][ansible-roles] about roles.
`ansible.cfg`       | Ansible configuration file. Contains some parameters like the location of the inventory file. [An example file][ansible-cfg-example] is available on Github.
`hosts.example`     | Example of an inventory file. INI-like file that defines which hosts Ansible will be running against. [Read more][ansible-inventory] about inventory files.
`webservers.yml`    | Main playbook. This file defines which roles and in which order Ansible should run them. [Read more][ansible-playbooks] about playbooks.


## References

- [Ansible Documentation](https://docs.ansible.com/ansible/latest/index.html)
- [Servers for Hackers – An Ansible Tutorial](https://serversforhackers.com/c/an-ansible-tutorial)
- [Ryan Eschinger Consulting – Securing a Server with Ansible](https://ryaneschinger.com/blog/securing-a-server-with-ansible/)
- [xdeb.org – My first 2 minutes on a server - letting Ansible do the work](https://xdeb.org/post/2016/06/23/my-first-2-minutes-on-a-server---letting-ansible-do-the-work/)
- [A curated list of Docker resources and projects on GitHub](https://github.com/veggiemonk/awesome-docker)
- [H5BP's Nginx configuration snippets on GitHub](https://github.com/h5bp/server-configs-nginx/tree/master/h5bp)
- [Calazan.com – Using SSH Agent Forwarding with Ansible](https://www.calazan.com/using-ssh-agent-forwarding-with-ansible/)
- [GitHub Help – Generating a new SSH key and adding it to the ssh-agent](https://help.github.com/articles/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent/)


[^1]: Docker-based role or action

[ansible]: https://www.ansible.com/
[ansible-cfg-example]: https://raw.github.com/ansible/ansible/devel/examples/ansible.cfg
[ansible-inventory]: http://docs.ansible.com/ansible/latest/intro_inventory.html
[ansible-playbooks]: http://docs.ansible.com/ansible/latest/user_guide/playbooks.html
[ansible-playbooks-variables]: http://docs.ansible.com/ansible/latest/playbooks_variables.html
[ansible-roles]: http://docs.ansible.com/ansible/latest/playbooks_reuse_roles.html
